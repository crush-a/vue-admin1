import {http} from "./index"

export default function login(data){
    return  http('login','post',data)
    // return是因为你的http是一个promise   不return的话 当你调用的时候后面就不能.then了
}